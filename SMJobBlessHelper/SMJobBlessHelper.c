/*
 
    File: SMJobBlessHelper.c
Abstract: A helper tool that doesn't do anything event remotely interesting.
See the ssd sample for how to use GCD and launchd to set up an on-demand
server via sockets.
 Version: 1.5

Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
Inc. ("Apple") in consideration of your agreement to the following
terms, and your use, installation, modification or redistribution of
this Apple software constitutes acceptance of these terms.  If you do
not agree with these terms, please do not use, install, modify or
redistribute this Apple software.

In consideration of your agreement to abide by the following terms, and
subject to these terms, Apple grants you a personal, non-exclusive
license, under Apple's copyrights in this original Apple software (the
"Apple Software"), to use, reproduce, modify and redistribute the Apple
Software, with or without modifications, in source and/or binary forms;
provided that if you redistribute the Apple Software in its entirety and
without modifications, you must retain this notice and the following
text and disclaimers in all such redistributions of the Apple Software.
Neither the name, trademarks, service marks or logos of Apple Inc. may
be used to endorse or promote products derived from the Apple Software
without specific prior written permission from Apple.  Except as
expressly stated in this notice, no other rights or licenses, express or
implied, are granted by Apple herein, including but not limited to any
patent rights that may be infringed by your derivative works or by other
works in which the Apple Software may be incorporated.

The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.

IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

Copyright (C) 2013 Apple Inc. All Rights Reserved.
*/

#include <syslog.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <xpc/xpc.h>


static void helper_peer_event_handler(xpc_connection_t connection, xpc_object_t event) {
    #pragma unused(connection)
    
    syslog(LOG_NOTICE, "Received event in helper");
    
    xpc_type_t type = xpc_get_type(event);
    
    if (type == XPC_TYPE_ERROR) {
        if (event == XPC_ERROR_CONNECTION_INVALID) {
            syslog(LOG_NOTICE, "XPC connection invalid");
        } else if (event == XPC_ERROR_TERMINATION_IMMINENT){
            syslog(LOG_NOTICE, "XPC connection termination");
        } else {
            syslog(LOG_NOTICE, "Unexpected XPC connection error");
        }
        return;
    } else {
        xpc_connection_t remote = xpc_dictionary_get_remote_connection(event);
        
        xpc_object_t reply = xpc_dictionary_create_reply(event);
        
        char pidStr[80];
        sprintf(pidStr, "Hi there, host applications: %d", getpid());
        xpc_dictionary_set_string(reply, "reply", pidStr);
        xpc_connection_send_message(remote, reply);
        xpc_release(reply);
    }
    
}
static void helper_connection_handler(xpc_connection_t connection) {
    syslog(LOG_NOTICE, "Configuring message event handler for helper");
    
    xpc_connection_set_event_handler(connection, ^(xpc_object_t event) {
        helper_peer_event_handler(connection, event);
    });
    
    xpc_connection_resume(connection);
}

int main(int argc, const char * argv[])
{
    #pragma unused(argc)
    #pragma unused(argv)

	syslog(LOG_NOTICE, "Hello world! uid = %d, euid = %d, pid = %d\n", (int) getuid(), (int) geteuid(), (int) getpid());
	
    xpc_connection_t service = xpc_connection_create_mach_service("com.mspanywhere.agentHelper", dispatch_get_main_queue(), XPC_CONNECTION_MACH_SERVICE_LISTENER);
    if (!service) {
        syslog(LOG_ERR, "Failed to create MSP Anywhere service");
        exit(EXIT_FAILURE);
    }
    
    xpc_connection_set_event_handler(service, ^(xpc_object_t connection) {
        helper_connection_handler(connection);
    });
    
    xpc_connection_resume(service);
    
    dispatch_main();
	return EXIT_SUCCESS;
}

